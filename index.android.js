/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import StarRating from 'react-native-star-rating';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  StatusBar,
  View
} from 'react-native';

export default class Test extends Component {
  onStarRatingPress(rating){}
  render() {
    return (
      <View style={styles.container}>

      <Image source={require('./img/goload-receipt_company.png')}
                    style={styles.container}>
                    <StatusBar
                       translucent = {true}
                       backgroundColor = 'rgba(255, 255, 255, 0)'
                     />
              <View style={styles.screenContainer}>
                          <View style={styles.topBox}>
                                <View style={styles.statusBar}>
                                      <Text style={styles.statusBarHeader}>
                                              Trip
                                      </Text>
                                </View>
                                <View style={styles.dateContainer}>
                                      <Text style={styles.dateText}>
                                          June 27,2017 at 5:15 PM
                                      </Text>
                                </View>
                                <View style={styles.priceContainer}>
                                      <Text style={styles.priceText}>
                                          $275.00
                                      </Text>
                                </View>
                                <View style={styles.addressBox}>
                                      <View style={styles.adressKeyContainer}>
                                            <Text style={styles.adressKeyText}>
                                              From
                                            </Text>
                                            <Text style={styles.adressKeyText}>
                                              To
                                            </Text>
                                            <Text style={styles.adressKeyText}>
                                              Miles
                                            </Text>
                                            <Text style={styles.adressKeyText}>
                                              Time
                                            </Text>
                                      </View>
                                      <View style={styles.addressValueContainer}>
                                            <Text style={styles.adressText} numberOfLines={1}>
                                              4905 Caswell Ave, Austin, TX
                                            </Text>
                                            <Text style={styles.adressText}>
                                              612 Willard St, Houston, TX
                                            </Text>
                                            <Text style={styles.adressText}>
                                              800 Miles
                                            </Text>
                                            <Text style={styles.adressText}>
                                              3 Hours and 10 Minutes
                                            </Text>
                                      </View>
                                </View>
                            <View style={styles.driverLogoContainer}>
                                <Image source={require('./img/driver.png')}
                                  style={styles.driverimage}
                                  resizeMethod = 'auto' >
                                  </Image>

                                  <View style={styles.driverContent}>
                                        <Text style={styles.diverText}>
                                          James Thompson
                                        </Text>
                                        <View style={styles.driverImageContainer}>
                                        <Image source={require('./img/stars.png')}
                                                      style={styles.driverContentImage}>
                                        </Image>
                                        </View>
                                        <Text style={styles.diverText1}>
                                          W900 Kenworth - Flatbed Trailer
                                        </Text>
                                  </View>
                            </View>
                    </View>
                    <View style={styles.bottomBox}>

                              <Text style={styles.ratingText}>
                                Rate the trip
                              </Text>
                              <StarRating
                                disabled={false}
                                maxStars={5}
                                rating={5}
                                selectedStar={(rating) => this.onStarRatingPress(rating)}
                              />

                    </View>
              </View>
      </Image>

    </View>
    );
  }
}

const styles = StyleSheet.create({
container:{
            flex: 1,
            width: undefined,
            height: undefined,
          },
screenContainer:{
            flex: 1,

          },
topBox:{
          flex: 3,

       },
 statusBar: {

               flexDirection: 'row',
               height: 60,
              backgroundColor:'#3CB371',


            },
 statusBarHeader: {
                   flex: 1,
                   color: '#FFFFFF',
                   fontSize: 28,
                   textAlign: 'center',
                   marginTop:20,
                  },
bottomBox:{
            flex: 1,
            alignItems:'center',
            justifyContent:'center'
          },
ratingText:{
            flex:1,
            fontSize:25,
            textAlign: 'center',
            marginTop:50

          },
dateContainer:{
                    flex:1,
                    backgroundColor:'white',
                    marginBottom: 10,
                    marginTop:20,
                    justifyContent:'flex-start'
              },

dateText:{
          flex: 1,
          textAlign:'center',
          fontSize:25,
          color:'#66757f',
          justifyContent:'flex-start',


        },
priceContainer:{
                flex:2,
                backgroundColor:'white',
                marginBottom: 20,

              },
priceText:{
          flex: 1,
          textAlign:'center',
          fontSize:60,
          color:'#3CB371',

          },
addressBox:{
            flex:5,
            flexDirection:'row',


          },
adressKeyContainer:{
                      flex:1,
                      marginLeft:25,
                      justifyContent:'space-around'
                    },
addressValueContainer:{
                        flex:3,

                        justifyContent:'space-around',
                        marginRight:25,
                      },
adressText:{
            fontSize:22,

            color:'#66757f',

          },
adressKeyText:{
                fontSize:22,
                color:'#292f33'
},
driverLogoContainer:{
                      flex:4,
                      flexDirection:'row',

                      marginBottom:20
                    },

driverimage:{
              flex:1,
            resizeMode:'center',
              transform:[{scale:0.95}],
              width:300,
              height:130,
              marginBottom:50,
              marginTop:20,
              marginLeft:20

            },
driverContent:{
                  flex:3,
                  justifyContent:'flex-start',
                  alignItems:'flex-start'

                },
diverText:{
            fontSize:27,
            color:'#292f33',
            marginLeft:10,
            marginTop:30
          },

driverImageContainer:{
                        justifyContent:'flex-start',
                        alignItems:'flex-start',
                      },
driverContentImage:{
                    transform:[{scale:0.8}],
                    resizeMode:'center',
                    width:220,
                    height:30,
                    alignItems:'flex-start',
                    justifyContent:'flex-start',
                    marginRight:50
                  },
diverText1:{
              fontSize:19,
              marginLeft:10,
              marginBottom:25,
              color:'#66757f',
          },
});

AppRegistry.registerComponent('Test', () => Test);
